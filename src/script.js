import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'lil-gui'
import { BoxGeometry, BufferGeometry, Color, Mesh, MeshBasicMaterial, MeshStandardMaterial, Raycaster, Vector2, Vector3 } from 'three'

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

/**
 * Galaxy
 */
const params = {
    count: 1000,
    size: 0.1,
    radius: 5,
    branches: 3,
    spin: 1,
    randomness: 0.2,
    randomnessPower: 1,
    insideColor: '#ff6030',
    outsideColor: '#1b3984',
    raycastMode: 'tunnnel',
    rayDiameter: 0.1
}
gui.add(params, 'count').min(100).max(1000000).step(100).onFinishChange(createGalaxy)
gui.add(params, 'size').min(0.001).max(0.1).step(0.001).onFinishChange(createGalaxy)
gui.add(params, 'radius').min(0.01).max(20).step(0.01).onFinishChange(createGalaxy)
gui.add(params, 'branches').min(2).max(20).step(1).onFinishChange(createGalaxy)
gui.add(params, 'spin').min(-5).max(5).step(0.01).onFinishChange(createGalaxy)
gui.add(params, 'randomness').min(0).max(2).step(0.01).onFinishChange(createGalaxy)
gui.add(params, 'randomnessPower').min(1).max(5).step(0.1).onFinishChange(createGalaxy)
gui.addColor(params, 'insideColor').onFinishChange(createGalaxy)
gui.addColor(params, 'outsideColor').onFinishChange(createGalaxy)

let geometry = null;
let points = null;
let material = null;

function createGalaxy() {

    // Destroy old galaxy
    if (points !== null) {
        geometry.dispose()
        material.dispose()
        scene.remove(points)
    }

    console.log('Generate galaxy');
    geometry = new BufferGeometry()
    const positions = new Float32Array(params.count * 3)
    const colors = new Float32Array(params.count * 3)

    const colorInside = new Color(params.insideColor)
    const colorOutside = new Color(params.outsideColor)

    for (let i = 0; i < params.count; i++) {
        const i3 = i * 3

        const radius = Math.random() * params.radius
        const spinAngle = (radius * params.spin) / 2
        const branchAngle = (i % params.branches) / params.branches * 2 * Math.PI

        const randomX = Math.pow(Math.random(), params.randomnessPower) * (Math.random() < 0.5 ? 1 : - 1) * params.randomness * radius
        const randomY = Math.pow(Math.random(), params.randomnessPower) * (Math.random() < 0.5 ? 1 : - 1) * params.randomness * radius
        const randomZ = Math.pow(Math.random(), params.randomnessPower) * (Math.random() < 0.5 ? 1 : - 1) * params.randomness * radius
        positions[i3] = Math.cos(branchAngle + spinAngle) * radius + randomX
        positions[i3 + 1] = randomY
        positions[i3 + 2] = Math.sin(branchAngle + spinAngle) * radius + randomZ

        //Color

        const mixedColor = colorInside.clone()
        mixedColor.lerp(colorOutside, radius / params.radius)

        colors[i3] = mixedColor.r
        colors[i3 + 1] = mixedColor.g
        colors[i3 + 2] = mixedColor.b


    }

    geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3))
    geometry.setAttribute('color', new THREE.BufferAttribute(colors, 3))

    material = new THREE.PointsMaterial({
        size: params.size,
        sizeAttenuation: true,
        depthWrite: false,
        blending: THREE.AdditiveBlending,
        vertexColors: true
    })

    points = new THREE.Points(geometry, material)
    scene.add(points)

}
createGalaxy()

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
// camera.position.x = 3
camera.position.y = 3
// camera.position.z = 3
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () => {
    const elapsedTime = clock.getElapsedTime()

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()


/**
 * TEST 
 */
gui.add(params, 'raycastMode', ['tunnel', 'point', 'branch'])
gui.add(params, 'rayDiameter').min(0.1).max(1).step(.01).name('Ray Diameter')

const mouse = new Vector2();

window.addEventListener('mousemove', (e) => {
    mouse.x = e.clientX / sizes.width * 2 - 1
    mouse.y = - e.clientY / sizes.height * 2 + 1
})

const pointRaycastParticles = () => {
    if (!points) return
    const raycaster = new Raycaster()
    raycaster.params.Points.threshold = params.rayDiameter
    raycaster.setFromCamera(mouse, camera)

    const intersectPoint = raycaster.intersectObject(points)?.[0]?.point
    if (!intersectPoint) return

    let pointsPositions = points.geometry.attributes.position.array

    let raycastedAreaCoords = []
    let randomColor = { r: Math.random(), g: Math.random(), b: Math.random() }

    for (let i = 0; i < pointsPositions.length / 3; i++) {
        let i3 = i * 3
        let distance = intersectPoint.distanceTo({ x: pointsPositions[i3], y: pointsPositions[i3 + 1], z: pointsPositions[i3 + 2] })
        if (distance <= 1) {
            raycastedAreaCoords[i3] = randomColor.r
            raycastedAreaCoords[i3 + 1] = randomColor.g
            raycastedAreaCoords[i3 + 2] = randomColor.b
        }
    }


    let colorAttr = points.geometry.attributes.color.array.slice()

    for (let i = 0; i < raycastedAreaCoords.length; i++) {
        if (raycastedAreaCoords[i]) {
            colorAttr[i] = raycastedAreaCoords[i]
        }
    }
    geometry.setAttribute('color', new THREE.BufferAttribute(colorAttr, 3))
}

const tunnelRaycastParticles = () => {
    if (!points) return
    const raycaster = new Raycaster()
    raycaster.params.Points.threshold = params.rayDiameter
    raycaster.setFromCamera(mouse, camera)

    const intersects = raycaster.intersectObject(points)
    console.log(intersects);

    let colorAttr = points.geometry.attributes.color.array.slice()
    let positionAttr = points.geometry.attributes.position.array.slice()
    let randomColor = { r: Math.random(), g: Math.random(), b: Math.random() }

    // let intersectedPositions = []

    for (let i = 0; i < intersects.length; i++) {
        for (let j = 0; j < positionAttr.length / 3; j++) {
            let j3 = j * 3
            let vector = new Vector3(positionAttr[j3], positionAttr[j3 + 1], positionAttr[j + 2])
            if (intersects[i].point.distanceTo(vector) < 1) {
                console.log('!');
                colorAttr[j3] = randomColor.r
                colorAttr[j3 + 1] = randomColor.g
                colorAttr[j3 + 2] = randomColor.b
            }
        }
    }

    geometry.setAttribute('color', new THREE.BufferAttribute(colorAttr, 3))
}

window.addEventListener('click', () => {
    if (params.raycastMode === 'point') {
        pointRaycastParticles()
    } else {
        tunnelRaycastParticles()
    }
})
